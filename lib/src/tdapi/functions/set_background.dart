part of '../tdapi.dart';

class SetBackground extends TdFunction {
  /// Changes the background selected by the user; adds background to the list of installed backgrounds
  SetBackground({this.background, this.type, this.forDarkTheme});

  /// [background] The input background to use. Pass null to create a new filled backgrounds. Pass null to remove the current background
  InputBackground background;

  /// [type] Background type. Pass null to use default type of the remote background. Pass null to remove the current background
  BackgroundType type;

  /// [forDarkTheme] True, if the background is chosen for dark theme
  bool forDarkTheme;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  SetBackground.fromJson(Map<String, dynamic> json);

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "background": this.background == null ? null : this.background.toJson(),
      "type": this.type == null ? null : this.type.toJson(),
      "for_dark_theme": this.forDarkTheme,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'setBackground';

  @override
  String getConstructor() => CONSTRUCTOR;
}
