part of '../tdapi.dart';

class GetGroupCallStreamSegment extends TdFunction {
  /// Returns a file with a segment of a group call stream in a modified OGG format
  GetGroupCallStreamSegment({this.groupCallId, this.timeOffset, this.scale});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [timeOffset] Point in time when the stream segment begins; Unix timestamp in milliseconds
  int timeOffset;

  /// [scale] Segment duration scale; 0-1. Segment's duration is 1000/(2**scale) milliseconds
  int scale;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  GetGroupCallStreamSegment.fromJson(Map<String, dynamic> json);

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "time_offset": this.timeOffset,
      "scale": this.scale,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getGroupCallStreamSegment';

  @override
  String getConstructor() => CONSTRUCTOR;
}
