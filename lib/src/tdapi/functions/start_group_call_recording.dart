part of '../tdapi.dart';

class StartGroupCallRecording extends TdFunction {
  /// Starts recording of an active group call. Requires groupCall.can_be_managed group call flag
  StartGroupCallRecording({this.groupCallId, this.title});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [title] Group call recording title; 0-64 characters
  String title;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  StartGroupCallRecording.fromJson(Map<String, dynamic> json);

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "title": this.title,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'startGroupCallRecording';

  @override
  String getConstructor() => CONSTRUCTOR;
}
