part of '../tdapi.dart';

class UploadStickerFile extends TdFunction {
  /// Uploads a PNG image with a sticker; returns the uploaded file
  UploadStickerFile({this.userId, this.sticker});

  /// [userId] Sticker file owner; ignored for regular users
  int userId;

  /// [sticker] Sticker file to upload
  InputSticker sticker;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  UploadStickerFile.fromJson(Map<String, dynamic> json);

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "user_id": this.userId,
      "sticker": this.sticker == null ? null : this.sticker.toJson(),
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'uploadStickerFile';

  @override
  String getConstructor() => CONSTRUCTOR;
}
