part of '../tdapi.dart';

class StartGroupCallScreenSharing extends TdFunction {
  /// Starts screen sharing in a joined group call. Returns join response payload for tgcalls
  StartGroupCallScreenSharing({this.groupCallId, this.payload});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [payload] Group call join payload; received from tgcalls
  String payload;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  StartGroupCallScreenSharing.fromJson(Map<String, dynamic> json);

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "payload": this.payload,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'startGroupCallScreenSharing';

  @override
  String getConstructor() => CONSTRUCTOR;
}
