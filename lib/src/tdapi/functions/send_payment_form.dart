part of '../tdapi.dart';

class SendPaymentForm extends TdFunction {
  /// Sends a filled-out payment form to the bot for final verification
  SendPaymentForm(
      {this.chatId,
      this.messageId,
      this.paymentFormId,
      this.orderInfoId,
      this.shippingOptionId,
      this.credentials,
      this.tipAmount});

  /// [chatId] Chat identifier of the Invoice message
  int chatId;

  /// [messageId] Message identifier
  int messageId;

  /// [paymentFormId] Payment form identifier returned by getPaymentForm
  int paymentFormId;

  /// [orderInfoId] Identifier returned by validateOrderInfo, or an empty string
  String orderInfoId;

  /// [shippingOptionId] Identifier of a chosen shipping option, if applicable
  String shippingOptionId;

  /// [credentials] The credentials chosen by user for payment
  InputCredentials credentials;

  /// [tipAmount] Chosen by the user amount of tip in the smallest units of the currency
  int tipAmount;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  SendPaymentForm.fromJson(Map<String, dynamic> json);

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "chat_id": this.chatId,
      "message_id": this.messageId,
      "payment_form_id": this.paymentFormId,
      "order_info_id": this.orderInfoId,
      "shipping_option_id": this.shippingOptionId,
      "credentials":
          this.credentials == null ? null : this.credentials.toJson(),
      "tip_amount": this.tipAmount,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'sendPaymentForm';

  @override
  String getConstructor() => CONSTRUCTOR;
}
