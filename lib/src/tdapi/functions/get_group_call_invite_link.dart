part of '../tdapi.dart';

class GetGroupCallInviteLink extends TdFunction {
  /// Returns invite link to a voice chat in a public chat
  GetGroupCallInviteLink({this.groupCallId, this.canSelfUnmute});

  /// [groupCallId] Group call identifier
  int groupCallId;

  /// [canSelfUnmute] Pass true if the invite_link should contain an invite hash, passing which to joinGroupCall would allow the invited user to unmute themselves. Requires groupCall.can_be_managed group call flag
  bool canSelfUnmute;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  GetGroupCallInviteLink.fromJson(Map<String, dynamic> json);

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "group_call_id": this.groupCallId,
      "can_self_unmute": this.canSelfUnmute,
      "@extra": this.extra,
    };
  }

  static const CONSTRUCTOR = 'getGroupCallInviteLink';

  @override
  String getConstructor() => CONSTRUCTOR;
}
