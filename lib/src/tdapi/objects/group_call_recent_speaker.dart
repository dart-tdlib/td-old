part of '../tdapi.dart';

class GroupCallRecentSpeaker extends TdObject {
  /// Describes a recently speaking participant in a group call
  GroupCallRecentSpeaker({this.participantId, this.isSpeaking});

  /// [participantId] Group call participant identifier
  MessageSender participantId;

  /// [isSpeaking] True, is the user has spoken recently
  bool isSpeaking;

  /// Parse from a json
  GroupCallRecentSpeaker.fromJson(Map<String, dynamic> json) {
    this.participantId =
        MessageSender.fromJson(json['participant_id'] ?? <String, dynamic>{});
    this.isSpeaking = json['is_speaking'];
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "participant_id":
          this.participantId == null ? null : this.participantId.toJson(),
      "is_speaking": this.isSpeaking,
    };
  }

  static const CONSTRUCTOR = 'groupCallRecentSpeaker';

  @override
  String getConstructor() => CONSTRUCTOR;
}
