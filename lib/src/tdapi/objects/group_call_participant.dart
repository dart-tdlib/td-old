part of '../tdapi.dart';

class GroupCallParticipant extends TdObject {
  /// Represents a group call participant
  GroupCallParticipant(
      {this.participantId,
      this.audioSourceId,
      this.canEnableVideo,
      this.videoInfo,
      this.screenSharingVideoInfo,
      this.bio,
      this.isCurrentUser,
      this.isSpeaking,
      this.isHandRaised,
      this.canBeMutedForAllUsers,
      this.canBeUnmutedForAllUsers,
      this.canBeMutedForCurrentUser,
      this.canBeUnmutedForCurrentUser,
      this.isMutedForAllUsers,
      this.isMutedForCurrentUser,
      this.canUnmuteSelf,
      this.volumeLevel,
      this.order});

  /// [participantId] Identifier of the group call participant
  MessageSender participantId;

  /// [audioSourceId] User's audio channel synchronization source identifier
  int audioSourceId;

  /// [canEnableVideo] True, if the user can broadcast video or share screen
  bool canEnableVideo;

  /// [videoInfo] Information about user's video channel; may be null if there is no active video
  GroupCallParticipantVideoInfo videoInfo;

  /// [screenSharingVideoInfo] Information about user's screen sharing video channel; may be null if there is no active screen sharing video
  GroupCallParticipantVideoInfo screenSharingVideoInfo;

  /// [bio] The participant user's bio or the participant chat's description
  String bio;

  /// [isCurrentUser] True, if the participant is the current user
  bool isCurrentUser;

  /// [isSpeaking] True, if the participant is speaking as set by setGroupCallParticipantIsSpeaking
  bool isSpeaking;

  /// [isHandRaised] True, if the participant hand is raised
  bool isHandRaised;

  /// [canBeMutedForAllUsers] True, if the current user can mute the participant for all other group call participants
  bool canBeMutedForAllUsers;

  /// [canBeUnmutedForAllUsers] True, if the current user can allow the participant to unmute themselves or unmute the participant (if the participant is the current user)
  bool canBeUnmutedForAllUsers;

  /// [canBeMutedForCurrentUser] True, if the current user can mute the participant only for self
  bool canBeMutedForCurrentUser;

  /// [canBeUnmutedForCurrentUser] True, if the current user can unmute the participant for self
  bool canBeUnmutedForCurrentUser;

  /// [isMutedForAllUsers] True, if the participant is muted for all users
  bool isMutedForAllUsers;

  /// [isMutedForCurrentUser] True, if the participant is muted for the current user
  bool isMutedForCurrentUser;

  /// [canUnmuteSelf] True, if the participant is muted for all users, but can unmute themselves
  bool canUnmuteSelf;

  /// [volumeLevel] Participant's volume level; 1-20000 in hundreds of percents
  int volumeLevel;

  /// [order] User's order in the group call participant list. Orders must be compared lexicographically. The bigger is order, the higher is user in the list. If order is empty, the user must be removed from the participant list
  String order;

  /// Parse from a json
  GroupCallParticipant.fromJson(Map<String, dynamic> json) {
    this.participantId =
        MessageSender.fromJson(json['participant_id'] ?? <String, dynamic>{});
    this.audioSourceId = json['audio_source_id'];
    this.canEnableVideo = json['can_enable_video'];
    this.videoInfo = GroupCallParticipantVideoInfo.fromJson(
        json['video_info'] ?? <String, dynamic>{});
    this.screenSharingVideoInfo = GroupCallParticipantVideoInfo.fromJson(
        json['screen_sharing_video_info'] ?? <String, dynamic>{});
    this.bio = json['bio'];
    this.isCurrentUser = json['is_current_user'];
    this.isSpeaking = json['is_speaking'];
    this.isHandRaised = json['is_hand_raised'];
    this.canBeMutedForAllUsers = json['can_be_muted_for_all_users'];
    this.canBeUnmutedForAllUsers = json['can_be_unmuted_for_all_users'];
    this.canBeMutedForCurrentUser = json['can_be_muted_for_current_user'];
    this.canBeUnmutedForCurrentUser = json['can_be_unmuted_for_current_user'];
    this.isMutedForAllUsers = json['is_muted_for_all_users'];
    this.isMutedForCurrentUser = json['is_muted_for_current_user'];
    this.canUnmuteSelf = json['can_unmute_self'];
    this.volumeLevel = json['volume_level'];
    this.order = json['order'];
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "participant_id":
          this.participantId == null ? null : this.participantId.toJson(),
      "audio_source_id": this.audioSourceId,
      "can_enable_video": this.canEnableVideo,
      "video_info": this.videoInfo == null ? null : this.videoInfo.toJson(),
      "screen_sharing_video_info": this.screenSharingVideoInfo == null
          ? null
          : this.screenSharingVideoInfo.toJson(),
      "bio": this.bio,
      "is_current_user": this.isCurrentUser,
      "is_speaking": this.isSpeaking,
      "is_hand_raised": this.isHandRaised,
      "can_be_muted_for_all_users": this.canBeMutedForAllUsers,
      "can_be_unmuted_for_all_users": this.canBeUnmutedForAllUsers,
      "can_be_muted_for_current_user": this.canBeMutedForCurrentUser,
      "can_be_unmuted_for_current_user": this.canBeUnmutedForCurrentUser,
      "is_muted_for_all_users": this.isMutedForAllUsers,
      "is_muted_for_current_user": this.isMutedForCurrentUser,
      "can_unmute_self": this.canUnmuteSelf,
      "volume_level": this.volumeLevel,
      "order": this.order,
    };
  }

  static const CONSTRUCTOR = 'groupCallParticipant';

  @override
  String getConstructor() => CONSTRUCTOR;
}
