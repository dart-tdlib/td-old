part of '../tdapi.dart';

class UserFullInfo extends TdObject {
  /// Contains full information about a user
  UserFullInfo(
      {this.photo,
      this.isBlocked,
      this.canBeCalled,
      this.supportsVideoCalls,
      this.hasPrivateCalls,
      this.needPhoneNumberPrivacyException,
      this.bio,
      this.shareText,
      this.description,
      this.groupInCommonCount,
      this.commands});

  /// [photo] User profile photo; may be null
  ChatPhoto photo;

  /// [isBlocked] True, if the user is blocked by the current user
  bool isBlocked;

  /// [canBeCalled] True, if the user can be called
  bool canBeCalled;

  /// [supportsVideoCalls] True, if a video call can be created with the user
  bool supportsVideoCalls;

  /// [hasPrivateCalls] True, if the user can't be called due to their privacy settings
  bool hasPrivateCalls;

  /// [needPhoneNumberPrivacyException] True, if the current user needs to explicitly allow to share their phone number with the user when the method addContact is used
  bool needPhoneNumberPrivacyException;

  /// [bio] A short user bio
  String bio;

  /// [shareText] For bots, the text that is shown on the bot's profile page and is sent together with the link when users share the bot
  String shareText;

  /// [description] For bots, the text shown in the chat with the bot if the chat is empty
  String description;

  /// [groupInCommonCount] Number of group chats where both the other user and the current user are a member; 0 for the current user
  int groupInCommonCount;

  /// [commands] For bots, list of the bot commands
  List<BotCommand> commands;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  UserFullInfo.fromJson(Map<String, dynamic> json) {
    this.photo = ChatPhoto.fromJson(json['photo'] ?? <String, dynamic>{});
    this.isBlocked = json['is_blocked'];
    this.canBeCalled = json['can_be_called'];
    this.supportsVideoCalls = json['supports_video_calls'];
    this.hasPrivateCalls = json['has_private_calls'];
    this.needPhoneNumberPrivacyException =
        json['need_phone_number_privacy_exception'];
    this.bio = json['bio'];
    this.shareText = json['share_text'];
    this.description = json['description'];
    this.groupInCommonCount = json['group_in_common_count'];
    this.commands = List<BotCommand>.from((json['commands'] ?? [])
        .map((item) => BotCommand.fromJson(item ?? <String, dynamic>{}))
        .toList());
    this.extra = json['@extra'];
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "photo": this.photo == null ? null : this.photo.toJson(),
      "is_blocked": this.isBlocked,
      "can_be_called": this.canBeCalled,
      "supports_video_calls": this.supportsVideoCalls,
      "has_private_calls": this.hasPrivateCalls,
      "need_phone_number_privacy_exception":
          this.needPhoneNumberPrivacyException,
      "bio": this.bio,
      "share_text": this.shareText,
      "description": this.description,
      "group_in_common_count": this.groupInCommonCount,
      "commands": this.commands.map((i) => i.toJson()).toList(),
    };
  }

  static const CONSTRUCTOR = 'userFullInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}
