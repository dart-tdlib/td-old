part of '../tdapi.dart';

class GroupCallParticipantVideoInfo extends TdObject {
  /// Contains information about a group call participant's video channel
  GroupCallParticipantVideoInfo(
      {this.sourceGroups, this.endpointId, this.isPaused});

  /// [sourceGroups] List of synchronization source groups of the video
  List<GroupCallVideoSourceGroup> sourceGroups;

  /// [endpointId] Video channel endpoint identifier
  String endpointId;

  /// [isPaused] True if the video is paused. This flag needs to be ignored, if new video frames are received
  bool isPaused;

  /// Parse from a json
  GroupCallParticipantVideoInfo.fromJson(Map<String, dynamic> json) {
    this.sourceGroups = List<GroupCallVideoSourceGroup>.from(
        (json['source_groups'] ?? [])
            .map((item) =>
                GroupCallVideoSourceGroup.fromJson(item ?? <String, dynamic>{}))
            .toList());
    this.endpointId = json['endpoint_id'];
    this.isPaused = json['is_paused'];
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "source_groups": this.sourceGroups.map((i) => i.toJson()).toList(),
      "endpoint_id": this.endpointId,
      "is_paused": this.isPaused,
    };
  }

  static const CONSTRUCTOR = 'groupCallParticipantVideoInfo';

  @override
  String getConstructor() => CONSTRUCTOR;
}
