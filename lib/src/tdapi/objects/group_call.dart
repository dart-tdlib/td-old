part of '../tdapi.dart';

class GroupCall extends TdObject {
  /// Describes a group call
  GroupCall(
      {this.id,
      this.title,
      this.scheduledStartDate,
      this.enabledStartNotification,
      this.isActive,
      this.isJoined,
      this.needRejoin,
      this.canBeManaged,
      this.participantCount,
      this.loadedAllParticipants,
      this.recentSpeakers,
      this.isMyVideoEnabled,
      this.isMyVideoPaused,
      this.canStartVideo,
      this.muteNewParticipants,
      this.canChangeMuteNewParticipants,
      this.recordDuration,
      this.duration});

  /// [id] Group call identifier
  int id;

  /// [title] Group call title
  String title;

  /// [scheduledStartDate] Point in time (Unix timestamp) when the group call is supposed to be started by an administrator; 0 if it is already active or was ended
  int scheduledStartDate;

  /// [enabledStartNotification] True, if the group call is scheduled and the current user will receive a notification when the group call will start
  bool enabledStartNotification;

  /// [isActive] True, if the call is active
  bool isActive;

  /// [isJoined] True, if the call is joined
  bool isJoined;

  /// [needRejoin] True, if user was kicked from the call because of network loss and the call needs to be rejoined
  bool needRejoin;

  /// [canBeManaged] True, if the current user can manage the group call
  bool canBeManaged;

  /// [participantCount] Number of participants in the group call
  int participantCount;

  /// [loadedAllParticipants] True, if all group call participants are loaded
  bool loadedAllParticipants;

  /// [recentSpeakers] Recently speaking users in the group call
  List<GroupCallRecentSpeaker> recentSpeakers;

  /// [isMyVideoEnabled] True, if the current user's video is enabled
  bool isMyVideoEnabled;

  /// [isMyVideoPaused] True, if the current user's video is paused
  bool isMyVideoPaused;

  /// [canStartVideo] True, if video can be enabled by group call participants. This flag is generally useless after the user has already joined the group call
  bool canStartVideo;

  /// [muteNewParticipants] True, if only group call administrators can unmute new participants
  bool muteNewParticipants;

  /// [canChangeMuteNewParticipants] True, if the current user can enable or disable mute_new_participants setting
  bool canChangeMuteNewParticipants;

  /// [recordDuration] Duration of the ongoing group call recording, in seconds; 0 if none. An updateGroupCall update is not triggered when value of this field changes, but the same recording goes on
  int recordDuration;

  /// [duration] Call duration; for ended calls only
  int duration;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  GroupCall.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.title = json['title'];
    this.scheduledStartDate = json['scheduled_start_date'];
    this.enabledStartNotification = json['enabled_start_notification'];
    this.isActive = json['is_active'];
    this.isJoined = json['is_joined'];
    this.needRejoin = json['need_rejoin'];
    this.canBeManaged = json['can_be_managed'];
    this.participantCount = json['participant_count'];
    this.loadedAllParticipants = json['loaded_all_participants'];
    this.recentSpeakers = List<GroupCallRecentSpeaker>.from(
        (json['recent_speakers'] ?? [])
            .map((item) =>
                GroupCallRecentSpeaker.fromJson(item ?? <String, dynamic>{}))
            .toList());
    this.isMyVideoEnabled = json['is_my_video_enabled'];
    this.isMyVideoPaused = json['is_my_video_paused'];
    this.canStartVideo = json['can_start_video'];
    this.muteNewParticipants = json['mute_new_participants'];
    this.canChangeMuteNewParticipants =
        json['can_change_mute_new_participants'];
    this.recordDuration = json['record_duration'];
    this.duration = json['duration'];
    this.extra = json['@extra'];
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "id": this.id,
      "title": this.title,
      "scheduled_start_date": this.scheduledStartDate,
      "enabled_start_notification": this.enabledStartNotification,
      "is_active": this.isActive,
      "is_joined": this.isJoined,
      "need_rejoin": this.needRejoin,
      "can_be_managed": this.canBeManaged,
      "participant_count": this.participantCount,
      "loaded_all_participants": this.loadedAllParticipants,
      "recent_speakers": this.recentSpeakers.map((i) => i.toJson()).toList(),
      "is_my_video_enabled": this.isMyVideoEnabled,
      "is_my_video_paused": this.isMyVideoPaused,
      "can_start_video": this.canStartVideo,
      "mute_new_participants": this.muteNewParticipants,
      "can_change_mute_new_participants": this.canChangeMuteNewParticipants,
      "record_duration": this.recordDuration,
      "duration": this.duration,
    };
  }

  static const CONSTRUCTOR = 'groupCall';

  @override
  String getConstructor() => CONSTRUCTOR;
}
