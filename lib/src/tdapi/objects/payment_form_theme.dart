part of '../tdapi.dart';

class PaymentFormTheme extends TdObject {
  /// Theme colors for a payment form
  PaymentFormTheme(
      {this.backgroundColor,
      this.textColor,
      this.hintColor,
      this.linkColor,
      this.buttonColor,
      this.buttonTextColor});

  /// [backgroundColor] A color of the payment form background in the RGB24 format
  int backgroundColor;

  /// [textColor] A color of text in the RGB24 format
  int textColor;

  /// [hintColor] A color of hints in the RGB24 format
  int hintColor;

  /// [linkColor] A color of links in the RGB24 format
  int linkColor;

  /// [buttonColor] A color of thebuttons in the RGB24 format
  int buttonColor;

  /// [buttonTextColor] A color of text on the buttons in the RGB24 format
  int buttonTextColor;

  /// Parse from a json
  PaymentFormTheme.fromJson(Map<String, dynamic> json) {
    this.backgroundColor = json['background_color'];
    this.textColor = json['text_color'];
    this.hintColor = json['hint_color'];
    this.linkColor = json['link_color'];
    this.buttonColor = json['button_color'];
    this.buttonTextColor = json['button_text_color'];
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "background_color": this.backgroundColor,
      "text_color": this.textColor,
      "hint_color": this.hintColor,
      "link_color": this.linkColor,
      "button_color": this.buttonColor,
      "button_text_color": this.buttonTextColor,
    };
  }

  static const CONSTRUCTOR = 'paymentFormTheme';

  @override
  String getConstructor() => CONSTRUCTOR;
}
