part of '../tdapi.dart';

class PaymentReceipt extends TdObject {
  /// Contains information about a successful payment
  PaymentReceipt(
      {this.title,
      this.description,
      this.photo,
      this.date,
      this.sellerBotUserId,
      this.paymentsProviderUserId,
      this.invoice,
      this.orderInfo,
      this.shippingOption,
      this.credentialsTitle,
      this.tipAmount});

  /// [title] Product title
  String title;

  /// [description] Product description
  String description;

  /// [photo] Product photo; may be null
  Photo photo;

  /// [date] Point in time (Unix timestamp) when the payment was made
  int date;

  /// [sellerBotUserId] User identifier of the seller bot
  int sellerBotUserId;

  /// [paymentsProviderUserId] User identifier of the payment provider bot
  int paymentsProviderUserId;

  /// [invoice] Contains information about the invoice
  Invoice invoice;

  /// [orderInfo] Order information; may be null
  OrderInfo orderInfo;

  /// [shippingOption] Chosen shipping option; may be null
  ShippingOption shippingOption;

  /// [credentialsTitle] Title of the saved credentials chosen by the buyer
  String credentialsTitle;

  /// [tipAmount] The amount of tip chosen by the buyer in the smallest units of the currency
  int tipAmount;

  /// callback sign
  dynamic extra;

  /// Parse from a json
  PaymentReceipt.fromJson(Map<String, dynamic> json) {
    this.title = json['title'];
    this.description = json['description'];
    this.photo = Photo.fromJson(json['photo'] ?? <String, dynamic>{});
    this.date = json['date'];
    this.sellerBotUserId = json['seller_bot_user_id'];
    this.paymentsProviderUserId = json['payments_provider_user_id'];
    this.invoice = Invoice.fromJson(json['invoice'] ?? <String, dynamic>{});
    this.orderInfo =
        OrderInfo.fromJson(json['order_info'] ?? <String, dynamic>{});
    this.shippingOption =
        ShippingOption.fromJson(json['shipping_option'] ?? <String, dynamic>{});
    this.credentialsTitle = json['credentials_title'];
    this.tipAmount = json['tip_amount'];
    this.extra = json['@extra'];
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "@type": CONSTRUCTOR,
      "title": this.title,
      "description": this.description,
      "photo": this.photo == null ? null : this.photo.toJson(),
      "date": this.date,
      "seller_bot_user_id": this.sellerBotUserId,
      "payments_provider_user_id": this.paymentsProviderUserId,
      "invoice": this.invoice == null ? null : this.invoice.toJson(),
      "order_info": this.orderInfo == null ? null : this.orderInfo.toJson(),
      "shipping_option":
          this.shippingOption == null ? null : this.shippingOption.toJson(),
      "credentials_title": this.credentialsTitle,
      "tip_amount": this.tipAmount,
    };
  }

  static const CONSTRUCTOR = 'paymentReceipt';

  @override
  String getConstructor() => CONSTRUCTOR;
}
